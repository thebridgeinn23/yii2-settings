<?php

namespace bridgeinn\settings;

use Yii;
/**
 * Class Module
 *
 * @package bridgeinn\settings
 */
class Module extends \yii\base\Module
{
    /**
     * @var string the namespace that controller classes are in
     */
    public $controllerNamespace = 'bridgeinn\settings\controllers';
    
    public $activeRecordClass = "yii\db\ActiveRecord";
    
    public function init() 
    {
        parent::init();
        Yii::$app->i18n->translations['bridgeinn.settings'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'sourceLanguage' => 'en',
            'basePath' => 'vendor/bridgeinn/yii2-settings/messages'
        ];
    } 
}
