<?php

namespace bridgeinn\settings\tests\data;

/**
 * Class Controller
 *
 * @package bridgeinn\settings\tests\data
 */
class Controller extends \yii\web\Controller
{
    /**
     * @inheritdoc
     */
    public function render($view, $params = [])
    {
        return [
            'view' => $view,
            'params' => $params,
        ];
    }
}
